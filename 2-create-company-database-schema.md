## Create Company Database Schema

### Create Employee Table

```sql
CREATE TABLE employee(
	emp_id INT PRIMARY KEY,
	first_name VARCHAR(20),
	last_name VARCHAR(20),
	birth_day DATE,
	sex VARCHAR(1),
	salary INT,
	super_id INT,
	branch_id INT,
	FOREIGN KEY(branch_id) REFERENCES branch(branch_id) ON DELETE SET NULL,
	FOREIGN KEY(super_id) REFERENCES employee(emp_id) ON DELETE SET NULL
)
```

### Alter Branch Table

```sql
ALTER TABLE branch
ADD FOREIGN KEY(mgr_id)
REFERENCES employee(emp_id) 
ON DELETE SET NULL;
```
### Alter Client Table

```sql
ALTER TABLE client 
ADD FOREIGN KEY(branch_id)
REFERENCES branch(branch_id)
ON DELETE SET NULL;
```

### Create Works_With table

```sql
CREATE TABLE works_with(
	emp_id INT,
	client_id INT,
	total_sales INT,
	PRIMARY KEY(emp_id, client_id),
	FOREIGN KEY(emp_id) REFERENCES employee(emp_id) ON DELETE CASCADE,
	FOREIGN KEY(client_id) REFERENCES client(client_id) ON DELETE CASCADE
);
```
### Create Branch Supplier table

```sql
CREATE TABLE branch_supplier(
	branch_id INT,
	supplier_name VARCHAR(30),
	supplier_type VARCHAR(30),
	PRIMARY KEY(branch_id, supplier_name),
	FOREIGN KEY(branch_id) REFERENCES branch(branch_id) ON DELETE CASCADE
);
```

