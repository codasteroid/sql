CREATE TABLE branch(
	branch_id INT PRIMARY KEY,
	branch_name VARCHAR(15),
	mgr_id INT,
	mgr_start_date DATE
)