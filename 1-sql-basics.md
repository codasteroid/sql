## SQL Basics

Most common data types:

* **INT**: whole numbers
* **DECIMAL(10, 4)**: decimal numbers
* **VARCHAR(5)**: string of text of length 5
* **BLOB**: binary large object, stores large data (files, images, etc)
* **DATE**: 'YYYY-MM-DD'
* **TIMESTAMP**: 'YYYY-MM-DD HH:MM:SS'

### Create Tables

To create a new database table:

```sql
CREATE TABLE student (
	student_id INT PRIMARY KEY,
	student_name VARCHAR(20),
	major VARCHAR(20)
);
```

Another way to define a primary key:

```sql
CREATE TABLE student (
	student_id INT,
	student_name VARCHAR(20),
	major VARCHAR(20),
	PRIMARY KEY(student_id)
);
```

## Alter Tables

To modify or change the structure of an existing database table:

```sql
ALTER TABLE student
ADD COLUMN gpa DECIMAL(3, 2)
```

## Insert Data 

To insert data entries into an existing database table:

```sql
INSERT INTO student VALUES(1, 'Jack', 'Geology')
```
Or:

```sql
INSERT INTO student(student_id, student_name, major)
VALUES(1, 'Jack', 'Geology')
```

## Delete Table

To delete an existing database table:

```sql
DROP TABLE student
```

To delete a column:

```sql
ALTER TABLE student
DROP COLUMN gpa
```

## Constraints

Constraints allow you to set specific rules for your database to follow in purpose to prevent data inconsistencies and errors. Common types of constraints in SQL include:

* **Unique Constraint**: it ensures that values are unique but allows NULL values.
* **Default Constraint**: It specifies a default value for a column, which is used when no value is provided during an `INSERT` operation.
* **Not Null Constraint**: it ensures that a column does not contain NULL values.

Example:

```sql
CREATE TABLE professor(
	professor_id SERIAL PRIMARY KEY,
	professor_name VARCHAR(20) UNIQUE,
	course VARCHAR(25) DEFAULT ('not provided')
);
``` 

## Update and Delete

To update an existing database table with new entries:

```sql
UPDATE student SET major = 'Astronomy'
WHERE major = 'Sociology' OR major = 'Geology'
```

You can change multiple columns within the same query:

```sql
UPDATE student SET major = 'Space Science', student_name = 'Yori' 
WHERE major = 'Astronomy'
```
