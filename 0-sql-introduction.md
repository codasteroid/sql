## SQL Basics

There are two types of PRIMARY keys:

* **Natural Key**: a primary key that has a mapping to something in real-world (e.g. SSN).
* **Surogate Key**: a primary key that has no mapping to something in real-world (e.g. random number).

SQL (Strutured Query Language) used to interact with Relational Database Management Systems (RDBMS). With SQL,
RDBMS can:

* Create, read, update, and delete data.
* Create and manage databases.
* Design and create database tables.
* Perform administration tasks (security,  user management, etc).

SQL implementation can vary between systems, not all RDBMS follow the SQL standards. However, the concepts
are the same.

SQL is a hybrid language, meaning it's basically 4 types of languages in one:

* **Data Query Language (DQL)**: Used to query database for information (get information that is already stored in the database).
* **Data Definition Language (DDL)**: Used to define database schemas (overal layout of the database). 
* **Data Control Language (DCL)**: Used for controlling access to the data in the database (user & permiqqion management).
* **Data Manipulation Language (DML)**: Used for inserting, updating, and deleting data from the database.


